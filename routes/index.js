// const User = require("../schema/userSchema");

require('dotenv').config();

const Koa = require('koa'); // ядро
const Router = require('koa-router'); // маршрутизация
const bodyParser = require('koa-bodyparser'); // парсер для POST запросов

const passport = require('koa-passport'); //passport for Koa
const LocalStrategy = require('passport-local'); //local Auth Strategy
const JwtStrategy = require('passport-jwt').Strategy; // Auth via JWT
const ExtractJwt = require('passport-jwt').ExtractJwt;

const crypto = require('crypto');

const app = new Koa();
const router = new Router();
const cors = require('koa-cors');
app.use(bodyParser());
app.use(cors());
app.use(passport.initialize()); // initialize passport first
app.use(router.routes()); // then routes
const server = app.listen(3000);

const jwtsecret = "mysecretkey"; // ключ для подписи JWT
const jwt = require('jsonwebtoken'); // аутентификация по JWT для hhtp
const mongoose = require('mongoose'); // стандартная прослойка для работы с MongoDB
const socketioJwt = require('socketio-jwt');
const socketIO = require('socket.io');

mongoose.Promise = Promise; // Просим Mongoose использовать стандартные Промисы
mongoose.set('debug', true);  // Просим Mongoose писать все запросы к базе в консоль. Удобно для отладки кода
mongoose.connect(process.env.DB_CONNECT); // Подключаемся к базе test на локальной машине. Если базы нет, она будет создана автоматически.

const User = require('../schema/userSchema')
//----------Passport Local Strategy--------------//
passport.use(new LocalStrategy(
    {
        usernameField: 'login',
        passwordField: 'password',
        session: false
    },
    function (login, password, done) {
        User.findOne({login}, (err, user) => {
            if (err) {
                return done(err);
            }
            if (!user || !user.checkPassword(password)) {
                return done(null, false, {message: 'User does not exist or wrong password.'});
            }
            return done(null, user);
        });
    }
    )
);
//----------Passport JWT Strategy--------//
// Expect JWT in the http header

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtsecret
};


passport.use(new JwtStrategy(jwtOptions, function (payload, done) {
        User.findById(payload.id, (err, user) => {
            if (err) {
                return done(err)
            }
            if (user) {
                done(null, user)
            } else {
                done(null, false)
            }
        })
    })
);

//------------Routing---------------//
router.post('/login', async(ctx, next) => {
    console.log("start login");
    await passport.authenticate('local', function (err, user) {
        // console.log(err);
        // console.log(user);
        if (user == false) {
            ctx.body = "Login failed";
        } else {
            //--payload - info to put in the JWT
            const payload = {
                id: user.id,
                name: user.name,
                login: user.login
            };
            const token = jwt.sign(payload, jwtsecret); //JWT is created here

            ctx.body = {user: {name: user.name, userId: user._id}, token};
        }
    })(ctx, next);
});

router.post('/sign-in', async(ctx, next) => {
    try {
        console.log(ctx.request.body)
        let new_user = await User.create(ctx.request.body);
        ctx.status = 200;
    }
    catch (err) {
        ctx.status = 400;
        ctx.body = err;
    }
});

router.get('/check-token', async(ctx, next) => {

    await passport.authenticate('jwt', function (err, user) {
        if (user) {
            ctx.body = "hello " + user.name;
        } else {
            console.log("err", err)
        }
    } )(ctx, next)
});

router.get('/me', async(ctx, next) => {

    await passport.authenticate('jwt', function (err, user) {
        if (user) {
            ctx.body = {
                user: {name: user.name, userId: user._id}
            };
        } else {
            ctx.body = "No such user";
            console.log("err", err)
        }
    } )(ctx, next)
});

//---Socket Communication-----//
let io = socketIO(server,{cors: {
        origin: '*'
    }
});

const registerMessageHandlers = require('./handlers/messageHandlers')
const registerUserHandlers = require('./handlers/userHandlers')
const log = console.log;
const onConnection = (socket) => {
    // выводим сообщение о подключении пользователя
    log('User connected')

    // получаем название комнаты из строки запроса "рукопожатия"
    const { roomId } = socket.handshake.query
    // сохраняем название комнаты в соответствующем свойстве сокета
    socket.roomId = roomId

    // присоединяемся к комнате (входим в нее)
    socket.join(roomId)

    // регистрируем обработчики
    // обратите внимание на передаваемые аргументы
    registerMessageHandlers(io, socket)
    registerUserHandlers(io, socket)

    // обрабатываем отключение сокета-пользователя
    socket.on('disconnect', () => {
        // выводим сообщение
        log('User disconnected')
        // покидаем комнату
        socket.leave(roomId)
    })
}

io.on('connection', onConnection)


// io.on('connection', socketioJwt.authorize({
//     secret: jwtsecret,
//     timeout: 15000
// })).on('authenticated', function (socket) {
//
//     console.log('this is the name from the JWT: ' + socket.decoded_token.displayName);
//
//     socket.on("clientEvent", (data) => {
//         console.log(data);
//     })
// });


