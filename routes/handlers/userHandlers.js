const User = require('../../schema/userSchema')
const users = {
    1: { username: 'Alice', online: false },
    2: { username: 'Bob', online: false }
}
module.exports = (io, socket) => {
    const getUsers = () => {
        io.in(socket.roomId).emit('users', users)
    }

    const addUser = ({ username, userId }) => {
        User.updateOne({_id :userId}, {online:true}, (err, result) =>{
            getUsers()
        })
    }

    const removeUser = (userId) => {
        User.updateOne({_id :userId}, {online:false}, (err, result) =>{
            getUsers()
        })
    }

    socket.on('user:get', getUsers)
    socket.on('user:add', addUser)
    socket.on('user:leave', removeUser)
}
