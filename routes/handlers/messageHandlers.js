const { nanoid } = require('nanoid')
// настраиваем БД

const mongoose = require("mongoose");

const chat = new mongoose.Schema({
    // messageId: mongoose.Schema.Types.ObjectId,
    userId: mongoose.Schema.Types.ObjectId,
    senderName: String,
    text: String,
    // online: Boolean,
}, {
    timestamps: true
});
const Chat = mongoose.model('Chat', chat);
// записываем в БД начальные данные

module.exports = (io, socket) => {
    // обрабатываем запрос на получение сообщений
    const getMessages = () => {
        // получаем сообщения из БД

        const messages = Chat.find({}, (err, messages)=>{
            console.log(messages)
            io.in(socket.roomId).emit('messages', messages)
        })

    }

    // обрабатываем добавление сообщения
    // функция принимает объект сообщения
    const addMessage = (message) => {
        console.log(message)

        Chat.create({...message}, (err, result)=>{
            getMessages();
        })
    }

    // обрабатываем удаление сообщение
    // функция принимает id сообщения
    const removeMessage = (messageId) => {
        Chat.findByIdAndDelete(messageId)
        getMessages()
    }

    // регистрируем обработчики
    socket.on('message:get', getMessages)
    socket.on('message:add', addMessage)
    // socket.on('message:remove', removeMessage)
}

